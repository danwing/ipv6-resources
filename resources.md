# IPv6 resources

## Books / Documentation

* [https://github.com/becarpenter/book6](https://github.com/becarpenter/book6)
* [List of all IPv6 related RFCs](https://www.system.de/ipv6-rfc/)


## Cloud

* [AWS IPv6 gaps](https://github.com/DuckbillGroup/aws-ipv6-gaps/)
* [Introducing IPv6-only subnets and EC2 instances](https://aws.amazon.com/blogs/networking-and-content-delivery/introducing-ipv6-only-subnets-and-ec2-instances/)

## Statistics

* [Akamai](https://www.akamai.com/de/internet-station/cyber-attacks/state-of-the-internet-report/ipv6-adoption-visualization)
* [Cisco 6Lab](https://6lab.cisco.com)
* [Cloudflare Radar](https://radar.cloudflare.com/reports/ipv6)
* [Facebook](https://www.facebook.com/ipv6)
* [Google IPv6](https://www.google.com/ipv6)
* [Potaroo BGP](https://bgp.potaroo.net/index-v6.html)

## Tools

### CLI

* [sipcalc](https://github.com/sii/sipcalc)
* [aggregate6](https://github.com/job/aggregate6)
* [grepcidr](https://github.com/ryantig/grepcidr)

## Training / Certification

* [Hurricane Electric](https://ipv6.he.net/certification/)
* [RIPE IPv6 Webinars](https://learning.ripe.net/w/)
* [AFRINIC Academy](https://afrinic.academy/)

## Videos

* [Klara Mall - IPv6 am KIT: Erfahrungsbericht und aktueller Status (German)](https://opencast.hu-berlin.de/paella/ui/watch.html?id=2fdf575e-eec7-4131-88fa-4b8f39c0e6d6)
* [Free CCNA IPv6 Part 1](https://www.youtube.com/watch?v=ZNuXyOXae5U)
* [Free CCNA IPv6 Part 2](https://www.youtube.com/watch?v=BrTMMOXFhDU)

## Fun

* [IPv6 Excuses](https://ipv6excuses.com/)
* [IPv6 Bingo](https://ipv6bingo.com/)
* [NATs are good](https://www.youtube.com/watch?v=v26BAlfWBm8&)
